
variable "ssh_key_file" {
  default = ""
  type = string
}

variable "key_pair" {
  default = ""
  type = string
}
