# Define required providers
terraform {
  required_version = ">= 1.0.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.42.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "key_pair" {
  name       = var.key_pair
  public_key = file("${var.ssh_key_file}")
}
